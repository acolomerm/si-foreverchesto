using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class mobCostTextController : MonoBehaviour
{

    public GameObject cost;
    public GameObject cardInvokeController;
    // Start is called before the first frame update
    void Start()
    {

        cardInvokeController.GetComponent<cardInvokeController>().cardCost += AssignScriptableObjectCost;


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AssignScriptableObjectCost(int id)
    {
        if (id == -1)
        {
            this.cost.GetComponent<TMP_Text>().text = "";
        }

        for (int i = 0; i < GameManager.Instance.myTeam.Length; i++)
        {

            if (GameManager.Instance.myTeam[i].id == id)
            {
                CardsScriptable cardMob = GameManager.Instance.myTeam[i];
                this.cost.GetComponent<TMP_Text>().text = cardMob.jewelCost.ToString();
            }
        }
    }


}
