using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ManaController : MonoBehaviour
{

    public TMP_Text manaText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        manaText.text = GameManager.Instance.jewel.ToString();
    }
}
