using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{

    // Atributos
    public static GameManager Instance = null;
    public int level = 1;
    public bool runBattle;
    public int jewel;
    public bool zonaVenta;
    public bool canBuy;

    // Lista de posiciones
    public List<Vector3Int> enemyPosition = new List<Vector3Int>();
    public List<Vector3Int> aliesPosition = new List<Vector3Int>();


    public GameObject player;
    public Tilemap sceneTileMap;

    //  Lista de Cartas
    public List<CardsScriptable> allMobs = new List<CardsScriptable>();
    public CardsScriptable[] myTeam = new CardsScriptable[6];   

    // Listas que contienen los gameobjects que  estan en el tablero
    public List<GameObject> enemyMobs = new List<GameObject>();
    public List<GameObject> aliesMobs = new List<GameObject>();

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        runBattle = false;
        this.jewel = 10;
        this.canBuy = true;
    }

    // Start is called before the first frame update
    

}
