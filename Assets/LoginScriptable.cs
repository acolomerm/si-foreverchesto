using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LoginScriptable", menuName = "Login")]
public class LoginScriptable : ScriptableObject
{

    public string userName;

}
