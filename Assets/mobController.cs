using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Mono.Cecil;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Pathfinding))]
public class mobController : MonoBehaviour
{
    public GameObject player;
    Pathfinding m_Pathfinding;

    public Vector3Int origin;
    Vector3Int target;

    public bool moving;

    [SerializeField]
    bool verPath;
    [SerializeField]
    private Tilemap m_Tilemap;
    [SerializeField]
    Color m_PathColor;

    List<Vector3Int> m_Cells;
    List<Vector3Int> possibleTargets;

    bool m_EditMode;
    Vector3Int coordenadesTilemap;

    // Variables a controlar
    public CardsScriptable statSO;

    Sprite[] img;
    public int hpMax;
    public int currentHp;
    public int damage;
    float atackSpeed;
    float moveSpeed;
    int range;
    int jewelCost;
    public bool hero;
    bool enemy;

    bool hit;
    bool hitanimation;
    bool canAtack;




    private void Awake()
    {
        m_Pathfinding = GetComponent<Pathfinding>();
        m_Tilemap = GameManager.Instance.sceneTileMap;
        player = GameManager.Instance.player;
        m_Cells = new List<Vector3Int>();
        enemy = false;
    }


    void Start()
    {
        this.img = statSO.img;
        this.GetComponent<SpriteRenderer>().sprite = this.img[1];
        this.hpMax = statSO.hpMax;
        statSO.setCurrentHp();
        this.currentHp = statSO.currentHp;
        this.damage = statSO.damage;
        this.atackSpeed = statSO.atackSpeed;
        this.moveSpeed = statSO.moveSpeed;
        this.range = statSO.range;
        this.jewelCost = statSO.jewelCost;
        this.hero = statSO.hero;
        this.enemy = statSO.enemy;
        if (this.enemy)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }




    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.runBattle == true)
        {
            if (hitanimation)
                this.GetComponent<SpriteRenderer>().sprite = this.img[2];
            else
                this.GetComponent<SpriteRenderer>().sprite = this.img[1];

            if (this.currentHp <= 0)
            {
                Destroy(this.gameObject);
            }
            FindTarget();
        }
        if (!moving)
        {
            if (canAtack)
            {
                if (!hit)
                {
                    Atack();
                    StartCoroutine(RestartAtack());
                    StartCoroutine(hitanim());

                }
            }
            else
            {
                GetInRange();
            }
        }
    }


    public void FindTarget()
    {
        if (this.enemy)
        {
            possibleTargets = GameManager.Instance.aliesPosition;
            float minDistance = Mathf.Infinity;

            foreach (Vector3Int t in possibleTargets)
            {
                if (Vector3Int.Distance(t, origin) <= minDistance)
                {
                    Vector3Int neigbour;
                    for (int x = -1; x <= 1; x++)
                    {
                        for (int y = -1; y <= 1; y++)
                        {
                            neigbour = new Vector3Int(t.x + x, t.y + y, 0);
                            if (GameManager.Instance.aliesPosition.Contains(neigbour))
                                continue;
                            minDistance = Vector3.Distance(t, origin);
                            if (minDistance <= this.range + 0.7f)
                                canAtack = true;
                            else
                                canAtack = false;
                            target = t;
                        }
                    }
                }
            }
        }
        else
        {
            possibleTargets = GameManager.Instance.enemyPosition;
            float minDistance = Mathf.Infinity;

            foreach (Vector3Int t in possibleTargets)
            {
                if (Vector3Int.Distance(t, origin) <= minDistance)
                {
                    Vector3Int neigbour;
                    for (int x = -1; x <= 1; x++)
                    {
                        for (int y = -1; y <= 1; y++)
                        {
                            neigbour = new Vector3Int(t.x + x, t.y + y, 0);
                            if (GameManager.Instance.enemyPosition.Contains(neigbour))
                                continue;
                            minDistance = Vector3.Distance(t, origin);
                            if (minDistance <= this.range + 0.7f)
                                canAtack = true;
                            else
                                canAtack = false;
                            target = t;
                        }
                    }
                }
            }
        }

    }

    protected void MoveTowards(Vector3Int nextNode)
    {
        this.origin = nextNode;
        this.transform.position = new Vector3(nextNode.x + 0.5f, nextNode.y + 0.25f, 0);
        StartCoroutine(StoppedToMove());
    }

    IEnumerator StoppedToMove()
    {
        moving = true;
        yield return new WaitForSeconds(1 / moveSpeed);
        moving = false;
    }

    public void GetInRange()
    {

        if (this.currentHp <= 0)
        {
            Destroy(this.gameObject);
        }

        if (target == new Vector3Int(0, 0, 0))
            return;

        if (!moving)
        {
            ChangeTileColor(Color.white);
            m_Cells.Clear();
            m_Pathfinding.FindPath(origin, target, this.enemy, out m_Cells);
            if (m_Cells.Count == 0)
            {
                m_Pathfinding.FindPath(origin, HandleLockState(origin, target, this.enemy, m_Cells), this.enemy, out m_Cells);
            }
            ChangeTileColor(Color.white);
            List<Vector3> worldCoordinatesPath;
            m_Pathfinding.FromCellPathToWorldPath(m_Cells, out worldCoordinatesPath);


            List<Vector3> worldCoordinatesPathDebug;
            m_Pathfinding.FromCellPathToWorldPath(m_Cells, out worldCoordinatesPathDebug);


            if (worldCoordinatesPathDebug.Count == worldCoordinatesPath.Count)
            {
                worldCoordinatesPath.ForEach(delegate (Vector3 coordinates)
                {
                    if (!worldCoordinatesPathDebug.Contains(coordinates))

                        Debug.LogError("path not correct");
                });
            }
            else
            {

                Debug.LogError("path not correct");
            }

            if (verPath)
                ChangeTileColor(Color.red);

            //Aqui habia un 1, no se porque. 

            if (!GameManager.Instance.aliesPosition.Contains(m_Cells[1]) && !GameManager.Instance.enemyPosition.Contains(m_Cells[1]))
            {
                if (!enemy)
                    for (int i = 0; i < GameManager.Instance.aliesPosition.Count; i++)
                    {
                        if (GameManager.Instance.aliesPosition[i] == this.origin)
                        {
                            GameManager.Instance.aliesPosition.RemoveAt(i);
                            GameManager.Instance.aliesPosition.Add(m_Cells[1]);
                        }
                    }
                else
                {
                    for (int i = 0; i < GameManager.Instance.enemyPosition.Count; i++)
                    {
                        if (GameManager.Instance.enemyPosition[i] == this.origin)
                        {
                            GameManager.Instance.enemyPosition.RemoveAt(i);
                            GameManager.Instance.enemyPosition.Add(m_Cells[1]);
                        }
                    }
                }
                MoveTowards(m_Cells[1]);
            }
        }
    }

    private Vector3Int HandleLockState(Vector3Int origin, Vector3Int target, bool enemy, List<Vector3Int> m_Cells)
    {
        m_Pathfinding.FindBlockedPath(target, origin, this.enemy, out m_Cells);
        Vector3Int LastPosibleCell = Vector3Int.zero;
        int i = 0;
        while (LastPosibleCell == Vector3Int.zero && i < m_Cells.Count)
        {
            if (!(GameManager.Instance.aliesPosition.Contains(m_Cells[i]) || GameManager.Instance.enemyPosition.Contains(m_Cells[i])))
            {
                LastPosibleCell = m_Cells[i];
            }
            i++;
        }

        return LastPosibleCell;
    }

    private void ChangeTileColor(Color color)
    {
        foreach (Vector3Int cell in m_Cells)
        {
            m_Tilemap.SetTileFlags(cell, TileFlags.None);
            m_Tilemap.SetColor(cell, color);
        }
    }

    private void OnMouseDrag()
    {
        if (!GameManager.Instance.runBattle)
        {
            m_EditMode = true;
            coordenadesTilemap = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            this.transform.position = m_Tilemap.GetCellCenterWorld(coordenadesTilemap);
        }

    }

    private void OnMouseUp()
    {
        coordenadesTilemap = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        Debug.Log("CoordeandesTilemap "+ coordenadesTilemap);
        Debug.Log("Origin "+origin);
        if (!GameManager.Instance.runBattle && m_EditMode && m_Pathfinding.IsWalkableTile(coordenadesTilemap) && !GameManager.Instance.aliesPosition.Contains(coordenadesTilemap) && coordenadesTilemap.x < 0)
        {
            m_EditMode = false;
            GameManager.Instance.aliesPosition.Remove(this.origin);
            this.transform.position = m_Tilemap.GetCellCenterWorld(coordenadesTilemap);
            GameManager.Instance.aliesPosition.Add(coordenadesTilemap);
            origin = coordenadesTilemap;
        }
        else if (!GameManager.Instance.runBattle && m_EditMode && GameManager.Instance.aliesPosition.Contains(coordenadesTilemap) || (!m_Pathfinding.IsWalkableTile(coordenadesTilemap) || coordenadesTilemap.x >= 0) && GameManager.Instance.zonaVenta == false)
        {
            m_EditMode = false;
            this.transform.position = m_Tilemap.GetCellCenterWorld(origin);
        }
        else if (GameManager.Instance.zonaVenta == true)
        {
           
            Destroy(this.gameObject);
            GameManager.Instance.aliesPosition.Remove(origin);
            GameManager.Instance.jewel += this.jewelCost;
            GameManager.Instance.aliesMobs.Remove(this.gameObject);
            
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Sellable")
        {
            GameManager.Instance.zonaVenta = true;
        }
    }


    public void Atack()
    {
        
        if (!this.enemy)
        {
            for (int i = 0; i < GameManager.Instance.enemyMobs.Count(); i++)
            {

                GameObject enemy = GameManager.Instance.enemyMobs[i].gameObject;

                if (enemy.GetComponent<mobController>().origin == target)
                {
                    enemy.GetComponent<mobController>().currentHp -= this.damage;
                    if (enemy.GetComponent<mobController>().currentHp <= 0)
                    {
                        GameManager.Instance.enemyMobs.Remove(enemy);
                        GameManager.Instance.enemyPosition.Remove(enemy.GetComponent<mobController>().origin);
                    }
                }

            }
        }
        else
        {
            for (int i = 0; i < GameManager.Instance.aliesMobs.Count(); i++)
            {

                GameObject alie = GameManager.Instance.aliesMobs[i].gameObject;

                if (alie.GetComponent<mobController>().origin == target)
                {
                    alie.GetComponent<mobController>().currentHp -= this.damage;
                    if (alie.GetComponent<mobController>().currentHp <= 0)
                    {
                        GameManager.Instance.aliesMobs.Remove(alie);
                        GameManager.Instance.aliesPosition.Remove(alie.GetComponent<mobController>().origin);
                    }
                }

            }
        }

    }
    IEnumerator RestartAtack()
    {
        hit = true;
        yield return new WaitForSeconds(1 / atackSpeed);
        hit = false;
    }

    IEnumerator hitanim()
    {
        hitanimation = true;
        yield return new WaitForSeconds(1);
        hitanimation = false;
    }


}
