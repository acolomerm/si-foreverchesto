using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerNameController : MonoBehaviour
{

    public TMP_Text textPlayerName;
    public LoginScriptable user;

    // Start is called before the first frame update
    void Start()
    {

        textPlayerName.text = user.userName;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
