using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class cardInvokeController : MonoBehaviour
{
    public int id;
    public delegate void ScriptableObjectCard(int id);
    public event ScriptableObjectCard card;

    public delegate void ScriptableObjectCardCost(int id);
    public event ScriptableObjectCardCost cardCost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        cardCost?.Invoke(id);
    }


    private void OnMouseDown()
    {
        card?.Invoke(id);
        if (GameManager.Instance.canBuy == true)
        {
            this.GetComponent<SpriteRenderer>().sprite = null;
            this.id = -1;
        }
        
    }
}
