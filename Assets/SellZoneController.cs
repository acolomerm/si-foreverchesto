using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellZoneController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sellable" && collision.GetComponent<mobController>().hero == false)
        {
            Debug.Log("Vendible");
            GameManager.Instance.zonaVenta = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Sellable")
        {
            GameManager.Instance.zonaVenta = false;
        }
    }

}
